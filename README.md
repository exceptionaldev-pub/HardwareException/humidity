Tested on NodeMCU ESP 32

## First of all

Copy the "Adafruit_Sensor.h" file into:
Arduino => libraries => create folder "Adafruit_Sensor"

Download [Humidity Driver](http://downloads.arduino.cc/libraries/github.com/adafruit/DHT_sensor_library-1.3.7.zip) and then paste it in this path in Arduino Idea:

Sketch -> Inclide Library -> Add .ZIP Library

Run then DHT_Unified_Sensor in Examples.

Change the digital pin in `define DHTPIN 13`

Uncomment the type of sensor in use: (In our example **DHT11** is the type of it) and then comment other types.
